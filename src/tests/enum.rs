#[derive(Debug,PartialEq,serde::Deserialize)]
enum Custom {
	Unit,
	NewType(bool),
	Tuple(u8, u16),
	Struct{s: String, f: f32},
}

#[test]
fn test() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Test {
		result_ok: Result<String, u32>,
		result_err: Result<String, u32>,
		custom_unit_inline: Custom,
		custom_unit_newline: Custom,
		custom_newtype: Custom,
		custom_tuple: Custom,
		custom_struct: Custom,
	}

	let t: Test = crate::from_bytes(b"
		result_ok:
			Ok: # foo
				whatever
		result_err:
			Err: 10 # foo

		custom_unit_inline: Unit
		custom_unit_newline: # bar
			Unit # unit
		custom_newtype: # foo

			NewType: true
		custom_tuple:
		# bar
			Tuple:


				1 # bar

				8
		custom_struct:
			Struct: # bar
				# bar
				s: foo
				# bar
				f: 3.14
	").unwrap();
	assert_eq!(t, Test{
		result_ok: Ok("whatever".into()),
		result_err: Err(10),

		custom_unit_inline: Custom::Unit,
		custom_unit_newline: Custom::Unit,
		custom_newtype: Custom::NewType(true),
		custom_tuple: Custom::Tuple(1, 8),
		custom_struct: Custom::Struct{s: "foo".into(), f: 3.14},
	});
}

#[test]
fn key() {
	#[derive(Debug,Eq,PartialEq,Ord,PartialOrd,serde::Deserialize)]
	enum E { Foo, Bar, Baz, Valued(bool) }

	let t: std::collections::BTreeMap<E, bool> = crate::from_bytes(b"
		Foo: true
		Bar: false
	").unwrap();
	assert_eq!(t, [
		(E::Foo, true),
		(E::Bar, false),
	].into_iter().collect());
}
