#[test]
fn test() {
	let t: Vec<Option<String>> = crate::from_bytes(b"
		foo
		none # comment
		None
		# comment
		:
		koala
		:
			# comment
		: # comment
			# comment
	").unwrap();
	assert_eq!(t, [
		Some("foo".into()),
		Some("none".into()),
		Some("None".into()),
		None,
		Some("koala".into()),
		Some("# comment".into()),
		Some("# comment".into()),
	])
}

#[test]
fn dict() {
	let t: std::collections::BTreeMap<String,Option<String>> = crate::from_bytes(b"
		dog:
			# comment
		fish: # comment


		foo:
			foo
		koala:
		meerkat:
			# foo
			# bar
		minx: # comment
	").unwrap();
	assert_eq!(t, [
		("dog".into(), Some("# comment".into())),
		("fish".into(), None),
		("foo".into(), Some("foo".into())),
		("koala".into(), None),
		("meerkat".into(), Some("# foo\n# bar".into())),
		("minx".into(), None),
	].into_iter().collect())
}

#[test]
fn two_word() {
	let t: std::collections::BTreeMap<String,Option<String>> = crate::from_bytes(b"
		dog:
			none here
	").unwrap();
	assert_eq!(t, [
		("dog".into(), Some("none here".into())),
	].into_iter().collect())
}
