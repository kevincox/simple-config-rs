#[test]
fn test() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Test {
		numbers: Vec<i32>,
		strings: Vec<String>,
	}

	let t: Test = crate::from_bytes("
numbers: # Before
	1
	2
# between
	3 # inline
	# after
strings:
	#before
	foo
	foo bar # inline
	not-a-key: foo
").unwrap();
	assert_eq!(t, Test{
		numbers: vec![1, 2, 3],
		strings: vec!["foo".into(), "foo bar".into(), "not-a-key: foo".into()],
	})

}

#[test]
fn multi_line() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Test {
		strings: Vec<String>,
		dicts: Vec<std::collections::HashMap<String, u8>>,
	}

	let t: Test = crate::from_bytes("
		strings:
			single-line

			:
				multi-line
				string!

			single-line
		dicts:
			:
				foo: 1
				bar: 2
			:
				baz: 3
	").unwrap();
	assert_eq!(t, Test{
		strings: vec!["single-line".into(), "multi-line\nstring!".into(), "single-line".into()],
		dicts: vec![
			[("foo".into(), 1), ("bar".into(), 2)].into_iter().collect(),
			[("baz".into(), 3)].into_iter().collect(),
		],
	})

}
