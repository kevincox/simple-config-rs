#[test]
fn test() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Inner {
		xyz: String,
		a: u8,
	}

	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Outer {
		a: Inner,
		abc: Inner,
	}

	let t: Outer = crate::from_bytes(b"
		# Useless comment
		a:
			xyz: foo # Inline
			a: 9
			# After
		abc: # Before indent.
			a: 0
			# Between
			xyz:1
	").unwrap();
	assert_eq!(t, Outer{
		a: Inner{xyz: "foo".into(), a: 9},
		abc: Inner{xyz: "1".into(), a: 0},
	})
}

#[test]
fn int_keys() {
	let t: std::collections::BTreeMap<i8, String> = crate::from_bytes(b"
		1: foo
		2: bar
		-1: zop
	").unwrap();
	assert_eq!(t, [
		(-1, "zop".into()),
		(1, "foo".into()),
		(2, "bar".into()),
	].into_iter().collect())
}

#[test]
fn option_keys() {
	let t: std::collections::BTreeMap<Option<String>, u8> = crate::from_bytes(b"
		any: 3
		2: 1
		: 2
	").unwrap();
	assert_eq!(t, [
		(None, 2),
		(Some("2".into()), 1),
		(Some("any".into()), 3),
	].into_iter().collect())
}
