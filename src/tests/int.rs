#[test]
fn test() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Test {
		signed: i32,
		unsigned: u32,

		neg: i32,

		binary_k: u32,
		binary_t: u64,

		si_m: u32,
		si_e: u64,
	}

	let t: Test = crate::from_bytes("
		signed: -8 # inline
		unsigned: 16

		neg: -190

		binary_k: 2Ki
		binary_t: 16Ti

		si_m: 10M
		si_e: 17E
	").unwrap();

	assert_eq!(t, Test{
		signed: -8,
		unsigned: 16,

		neg: -190,

		binary_k: 2 * 2u32.pow(10),
		binary_t: 16 * 2u64.pow(40),

		si_m: 10_000_000,
		si_e: 17_000_000_000_000_000_000,
	})
}

#[test]
fn too_big() {
	#[derive(Debug,serde::Deserialize)]
	struct Test {
		_i: i32,
	}

	let e = crate::from_bytes::<Test, _>("
		_i: 2Ei
	").unwrap_err();

	assert_eq!(e.to_string(), "\"2Ei\" is too big at 2:7");
}

#[test]
fn overflows() {
	assert_eq!(
		crate::from_bytes::<u128, _>("340282366920938463463374607431768211455").unwrap(),
		u128::max_value());
	assert_eq!(
		crate::from_bytes::<u128, _>("340282366920938463463374607431768211456").unwrap_err().to_string(),
		"\"340282366920938463463374607431768211456\" is too big at 1:1");

	assert_eq!(
		crate::from_bytes::<u128, _>("3e38").unwrap(),
		300000000000000000000000000000000000000);
	assert_eq!(
		crate::from_bytes::<u128, _>("4e38").unwrap_err().to_string(),
		"\"4e38\" is too big at 1:1");

	assert_eq!(
		crate::from_bytes::<u128, _>(".3e39").unwrap(),
		300000000000000000000000000000000000000);
	assert_eq!(
		crate::from_bytes::<u128, _>(".4e39").unwrap_err().to_string(),
		"\".4e39\" is too big at 1:1");

	assert_eq!(
		crate::from_bytes::<i128, _>("170141183460469231731687303715884105727").unwrap(),
		i128::max_value());
	assert_eq!(
		crate::from_bytes::<i128, _>("170141183460469231731687303715884105728").unwrap_err().to_string(),
		"\"170141183460469231731687303715884105728\" is too big at 1:1");

	assert_eq!(
		crate::from_bytes::<i128, _>("-170141183460469231731687303715884105728").unwrap(),
		i128::min_value());
	assert_eq!(
		crate::from_bytes::<i128, _>("-170141183460469231731687303715884105729").unwrap_err().to_string(),
		"\"-170141183460469231731687303715884105729\" is too big at 1:1");
}

