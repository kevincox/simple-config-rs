#[test]
fn test_strings() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Test {
		a: String,
		bar: String,
	}

	let t: Test = crate::from_bytes("a: foobar\nbar: bazbaz\n").unwrap();
	assert_eq!(t, Test{a: "foobar".into(), bar: "bazbaz".into()})
}

#[test]
fn test_multiline_strings() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Test {
		tabs: String,
		spaces: String,
		only_comment: String,
	}

	let t: Test = crate::from_bytes("
		tabs:
			this is so
			# No comments
			cool
		spaces: # Comment
		    this is so
		    cool
		only_comment:
			# in string
	").unwrap();
	assert_eq!(t, Test{
		tabs: "this is so\n# No comments\ncool".into(),
		spaces: "this is so\ncool".into(),
		only_comment: "# in string".into(),
	})
}
