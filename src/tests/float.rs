#[test]
fn f32() {
	let t: Vec<f32> = crate::from_bytes("
		-2.1
		-10
		0
		1
		1.3
		0x20Mi
	").unwrap();

	assert_eq!(t, &[
		-2.1,
		-10.0,
		0.0,
		1.0,
		1.3,
		33554432.0,
	])
}

#[test]
fn f64() {
	let t: Vec<f64> = crate::from_bytes("
		-2.1
		-10
		0
		1
		1.3
		0x20Mi
	").unwrap();

	assert_eq!(t, &[
		-2.1,
		-10.0,
		0.0,
		1.0,
		1.3,
		33554432.0,
	])
}

#[derive(Debug,PartialEq,PartialOrd,serde::Deserialize)]
struct OrdFloat<T>(T);

impl<T: PartialEq> Eq for OrdFloat<T> {}

impl Ord for OrdFloat<f32> {
	fn cmp(&self, that: &Self) -> std::cmp::Ordering {
		self.partial_cmp(that)
			.unwrap_or(self.0.to_be_bytes().cmp(&that.0.to_be_bytes()))
	}
}


impl Ord for OrdFloat<f64> {
	fn cmp(&self, that: &Self) -> std::cmp::Ordering {
		self.partial_cmp(that)
			.unwrap_or(self.0.to_be_bytes().cmp(&that.0.to_be_bytes()))
	}
}


#[test]
fn f32_keys() {
	let t: std::collections::BTreeMap<OrdFloat<f32>, String> = crate::from_bytes("
		-2.2: foo
		0: bar
		0x20Mi: baz
	").unwrap();

	assert_eq!(t, [
		(OrdFloat(-2.2), "foo".into()),
		(OrdFloat(0.0), "bar".into()),
		(OrdFloat(33554432.0), "baz".into()),
	].into_iter().collect())
}

#[test]
fn f64_keys() {
	let t: std::collections::BTreeMap<OrdFloat<f64>, String> = crate::from_bytes("
		-2.2: foo
		0: bar
		0x20Mi: baz
	").unwrap();

	assert_eq!(t, [
		(OrdFloat(-2.2), "foo".into()),
		(OrdFloat(0.0), "bar".into()),
		(OrdFloat(33554432.0), "baz".into()),
	].into_iter().collect())
}
