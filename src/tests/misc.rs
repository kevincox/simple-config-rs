#[test]
fn empty_struct() {
	#[derive(Debug,PartialEq,serde::Deserialize)]
	struct Test {
		#[serde(default)]
		list: Vec<i32>,
		#[serde(default)]
		number: Option<u8>,
		#[serde(default)]
		string: String,
		#[serde(default)]
		unit: (),
	}

	let t: Test = crate::from_bytes("").unwrap();
	assert_eq!(t, Test{
		list: Default::default(),
		number: Default::default(),
		string: Default::default(),
		unit: Default::default(),
	});

	let t: String = crate::from_bytes(" ").unwrap();
	assert_eq!(t, "");

	let t: Vec<u8> = crate::from_bytes(" \n  \n \n  \n\n \n").unwrap();
	assert_eq!(t, vec![]);
}
