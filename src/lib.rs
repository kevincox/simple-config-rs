#![doc = include_str!("../README.md")]

mod de; pub use de::*;

#[cfg(test)] mod tests;

fn from_utf8(s: &[u8], mut location: Location) -> Result<&str, DeserializeError> {
	std::str::from_utf8(s)
		.map_err(|e| {
			if let Some(last_newline_rev) = &s[..e.valid_up_to()].iter().rev().position(|c| *c == b'\n') {
				let last_newline = e.valid_up_to() - last_newline_rev;
				location.add_lines(s[..last_newline].iter().filter(|c| **c == b'\n').count());
				location.add_columns(last_newline - e.valid_up_to());
			} else {
				location.add_columns(e.valid_up_to());
			}
			crate::DeserializeErrorKind::InvalidUtf8.at(location)
		})
}

unsafe fn from_utf8_unchecked(s: &[u8]) -> &str {
	debug_assert!(std::str::from_utf8(s).is_ok(), "Assumed string was valid UTF-8 {:?}", String::from_utf8_lossy(s));
	std::str::from_utf8_unchecked(s)
}
