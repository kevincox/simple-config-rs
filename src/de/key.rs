pub(crate) struct KeyDeserializer<'a, R: buffered_reader::BufferedReader<()>>{
	pub root: &'a mut crate::Deserializer<R>,
}

impl<'a, R: buffered_reader::BufferedReader<()>> KeyDeserializer<'a, R> {
	fn take_sint<T: std::convert::TryFrom<i128> + std::ops::Neg>(self) -> Result<T, crate::DeserializeError>
	where
		<T as std::convert::TryFrom<i128>>::Error: std::fmt::Debug,
		<T as std::ops::Neg>::Output: std::convert::Into<T>,
	{
		let (s, location) = self.root.parse_key()?;
		crate::parse_num(&s, location)?.to_sint(&s).map_err(|e| e.at(location))
	}

	fn take_uint<T: std::convert::TryFrom<u128>>(self) -> Result<T, crate::DeserializeError>
	where
		<T as std::convert::TryFrom<u128>>::Error: std::fmt::Debug,
	{
		let (s, location) = self.root.parse_key()?;
		crate::parse_num(&s, location)?.to_uint(&s).map_err(|e| e.at(location))
	}

	fn take_float(self) -> Result<f64, crate::DeserializeError> {
		let (s, location) = self.root.parse_key()?;
		crate::parse_num(&s, location).map(|n| n.to_f64())
	}
}

impl<'d, 'a, R: buffered_reader::BufferedReader<()>> serde::de::Deserializer<'d> for KeyDeserializer<'a, R> {
	type Error = crate::DeserializeError;

	fn deserialize_any<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_str(visitor)
	}

	fn deserialize_bool<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		let (s, location) = self.root.parse_key()?;
		let b = match s.as_ref() {
			"true" => true,
			"false" => false,
			other => return Err(
				crate::DeserializeErrorKind::Invalid(
					format!("Expected bool, got {:?}", other))
					.at(location)),
		};
		visitor.visit_bool(b)
	}

	fn deserialize_i8<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_i8(self.take_sint()?)
	}

	fn deserialize_i16<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_i16(self.take_sint()?)
	}

	fn deserialize_i32<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_i32(self.take_sint()?)
	}

	fn deserialize_i64<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_i64(self.take_sint()?)
	}

	fn deserialize_i128<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_i128(self.take_sint()?)
	}

	fn deserialize_u8<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_u8(self.take_uint()?)
	}

	fn deserialize_u16<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_u16(self.take_uint()?)
	}

	fn deserialize_u32<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_u32(self.take_uint()?)
	}

	fn deserialize_u64<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_u64(self.take_uint()?)
	}

	fn deserialize_u128<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_u128(self.take_uint()?)
	}

	fn deserialize_f32<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_f32(self.take_float()? as f32)
	}

	fn deserialize_f64<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_f64(self.take_float()?)
	}

	fn deserialize_char<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		let (s, location) = self.root.parse_key()?;
		if s.len() == 1 {
			visitor.visit_char(s.chars().next().unwrap())
		} else {
			Err(crate::DeserializeErrorKind::Invalid(
				format!("Expected single character, got {:?}", s))
				.at(location))
		}
	}

	fn deserialize_str<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_str(&self.root.parse_key()?.0)
	}

	fn deserialize_string<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_byte_buf(visitor)
	}

	fn deserialize_byte_buf<V: serde::de::Visitor<'d>>(self, _visitor: V) -> Result<V::Value, crate::DeserializeError> {
		Err(
			crate::DeserializeErrorKind::Unimplemented("deserialize_byte_buf")
				.at(self.root.location))
	}

	fn deserialize_option<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		if self.root.input.data(1)?.starts_with(b":") {
			let (k, _) = self.root.parse_key()?;
			debug_assert_eq!(k, "");
			visitor.visit_none()
		} else {
			visitor.visit_some(self)
		}
	}

	fn deserialize_unit<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		self.root.parse_key()?;
		visitor.visit_unit()
	}

	fn deserialize_unit_struct<V: serde::de::Visitor<'d>>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_unit(visitor)
	}

	fn deserialize_newtype_struct<V: serde::de::Visitor<'d>>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, crate::DeserializeError> {
		visitor.visit_newtype_struct(self)
	}

	fn deserialize_seq<V: serde::de::Visitor<'d>>(self, _visitor: V) -> Result<V::Value, crate::DeserializeError> {
		Err(
			crate::DeserializeErrorKind::Invalid(
				"Keys can't be sequences".to_string())
				.at(self.root.location))
	}

	fn deserialize_tuple<V: serde::de::Visitor<'d>>(self, _len: usize, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_seq(visitor)
	}

	fn deserialize_tuple_struct<V: serde::de::Visitor<'d>>(
		self,
		_name: &'static str,
		_len: usize,
		visitor: V,
	) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_seq(visitor)
	}

	fn deserialize_map<V: serde::de::Visitor<'d>>(self, _visitor: V) -> Result<V::Value, crate::DeserializeError> {
		Err(
			crate::DeserializeErrorKind::Invalid(
				"Keys can't be maps".to_string())
				.at(self.root.location))
	}

	fn deserialize_struct<V: serde::de::Visitor<'d>>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_map(visitor)
	}

	fn deserialize_enum<V: serde::de::Visitor<'d>>(
		self,
		name: &'static str,
		variants: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, crate::DeserializeError> {
		let (s, location) = self.root.parse_key()?;
		serde::de::IntoDeserializer::into_deserializer(s)
			.deserialize_enum(name, variants, visitor)
			.map_err(|e: crate::DeserializeError| e.with_location(location))
	}

	fn deserialize_identifier<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_str(visitor)
	}

	fn deserialize_ignored_any<V: serde::de::Visitor<'d>>(self, visitor: V) -> Result<V::Value, crate::DeserializeError> {
		self.deserialize_unit(visitor)
	}
}
