pub(crate) struct MapDeserializer<'a, T: buffered_reader::BufferedReader<()>> {
	pub root: &'a mut crate::Deserializer<T>,
}

impl<'d, 'a, T: buffered_reader::BufferedReader<()>> serde::de::MapAccess<'d> for MapDeserializer<'a, T> {
	type Error = crate::DeserializeError;

	fn next_key_seed<K: serde::de::DeserializeSeed<'d>>(&mut self, seed: K) -> Result<Option<K::Value>, crate::DeserializeError> {
		match self.root.read_line(true)? {
			crate::ReadResult::Inline => {
				return Err(crate::DeserializeErrorKind::Invalid(
					"Inline string not allowed for dict".into())
					.at(self.root.location))
			}
			crate::ReadResult::None => {
				return Ok(None)
			}
			crate::ReadResult::Multiline => {
				// Fall through
			}
		}

		seed.deserialize(crate::KeyDeserializer {
			root: self.root,
		}).map(Some)
	}

	fn next_value_seed<K: serde::de::DeserializeSeed<'d>>(&mut self, seed: K) -> Result<K::Value, crate::DeserializeError> {
		seed.deserialize(&mut *self.root)
	}
}
