#[derive(Copy,Clone,Debug,Eq,PartialEq)]
pub struct Location {
	pub line: usize,
	pub column: usize,
}

impl Location {
	pub(crate) fn add_lines(&mut self, n: usize) {
		self.line += n;
		self.column = 1;
	}

	pub(crate) fn add_columns(&mut self, n: usize) {
		self.column += n;
	}
}

impl std::fmt::Display for Location {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}:{}", self.line, self.column)
	}
}
