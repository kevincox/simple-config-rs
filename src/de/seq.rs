pub(crate) struct SeqDeserializer<'a, T: buffered_reader::BufferedReader<()>> {
	pub root: &'a mut crate::Deserializer<T>,
}

impl<'d, 'a, T: buffered_reader::BufferedReader<()>> serde::de::SeqAccess<'d> for SeqDeserializer<'a, T> {
	type Error = crate::DeserializeError;

	fn next_element_seed<K: serde::de::DeserializeSeed<'d>>(&mut self, seed: K) -> Result<Option<K::Value>, crate::DeserializeError> {
		match self.root.read_line(true)? {
			crate::ReadResult::Inline => {
				return Err(crate::DeserializeErrorKind::Invalid(
					format!("Inline string not allowed for list"))
					.at(self.root.location))
			}
			crate::ReadResult::None => {
				return Ok(None)
			}
			crate::ReadResult::Multiline => {
				// Fall through
			}
		}

		// A `:` in a list introduces a multi-line block. We simply leave the input stream at the newline after the `:`. The main Deserializer treats this as opening a multi-line block.
		if self.root.input.data(1)?.starts_with(b":") {
			self.root.input.consume(1);
			self.root.location.add_columns(1);
			let b = self.root.input.read_to(b'\n')?;
			let len = b.len();
			let s = crate::from_utf8(b, self.root.location)?;
			if let Some(i) = s.find(|c: char| !c.is_whitespace()) {
				if s.as_bytes()[i] != b'#' {
					return Err(crate::DeserializeErrorKind::Invalid(
						format!("Expected newline after `:` found {:?}", s))
						.at(self.root.location))
				}
			}
			self.root.input.consume(len);
			self.root.location.add_lines(1);

			self.root.position = crate::Position::Newline;
			let r = seed.deserialize(&mut *self.root);
			r.map(Some)
		} else {
			let b = self.root.input.read_to(b'\n')?;
			let end = b.len();
			if let Some('#') = bstr::ByteSlice::chars(b).find(|c| !c.is_whitespace()) {
				self.root.input.consume(end);
				self.root.location.add_lines(1);
				return self.next_element_seed(seed)
			}

			self.root.position = crate::Position::Inline;
			seed.deserialize(&mut *self.root).map(Some)
		}
	}
}
