#[derive(Debug,thiserror::Error)]
#[non_exhaustive]
pub struct DeserializeError {
	#[source]
	pub kind: DeserializeErrorKind,
	pub location: Option<crate::Location>,
}

impl DeserializeError {
	pub(crate) fn with_location(mut self, location: crate::Location) -> Self {
		if self.location.is_none() {
			self.location = Some(location);
		}
		self
	}
}

impl std::fmt::Display for DeserializeError {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		if let Some(location) = self.location {
			write!(f, "{} at {}", self.kind, location)
		} else {
			write!(f, "{}", self.kind)
		}
	}
}

impl serde::de::Error for DeserializeError {
	fn custom<T: std::fmt::Display>(msg: T) -> Self {
		DeserializeError {
			kind: DeserializeErrorKind::Custom(format!("{}", msg)),
			location: None,
		}
	}
}

impl From<std::io::Error> for DeserializeError {
	fn from(e: std::io::Error) -> Self {
		DeserializeError {
			kind: DeserializeErrorKind::Io(e),
			location: None,
		}
	}
}

#[non_exhaustive]
#[derive(Debug,thiserror::Error)]
pub enum DeserializeErrorKind {
	#[error("{0}")]
	Custom(String),
	#[error("{0}")]
	Invalid(String),
	#[error("Expected integer, got {0:?}")]
	IntFraction(String),
	#[error("{0:?} is too big")]
	IntOverflow(String),
	#[error("Expected unsigned integer, got -{0}")]
	IntSigned(u128),
	#[error("Unexpected characters after number {0:?}")]
	NumTrailing(String),
	#[error("e exponents only supported in base 10")]
	NumExpBase,
	#[error("Could not parse exponent as integer: {0}")]
	NumExpInvalid(#[source] std::num::ParseIntError),
	#[error(transparent)]
	Io(#[from] std::io::Error),
	#[error("{0} is not supported.")]
	Unimplemented(&'static str),
	#[error("Invalid UTF-8")]
	InvalidUtf8,
}

impl DeserializeErrorKind {
	pub(crate) fn at(self, location: crate::Location) -> DeserializeError {
		DeserializeError {
			kind: self,
			location: Some(location),
		}
	}
}
