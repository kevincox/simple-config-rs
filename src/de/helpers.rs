pub fn from_bytes<'d, T: serde::Deserialize<'d>, B: AsRef<[u8]>>(b: B) -> Result<T, crate::DeserializeError> {
	let mut d = crate::Deserializer::from(buffered_reader::Memory::new(b.as_ref()));
	T::deserialize(&mut d)
}

pub fn from_file<'d, T: serde::Deserialize<'d>, P: AsRef<std::path::Path>>(p: P) -> Result<T, crate::DeserializeError> {
	let f = buffered_reader::File::open(p)?;
	let mut d = crate::Deserializer::from(f);
	T::deserialize(&mut d)
}

pub fn from_reader<
	'd,
	T: serde::Deserialize<'d>,
	R: std::io::Read + Send + Sync
> (
	r: R
) -> Result<T, crate::DeserializeError>
{
	let f = buffered_reader::Generic::new(r, None);
	let mut d = crate::Deserializer::from(f);
	T::deserialize(&mut d)
}
