pub(crate) struct EnumDeserializer<'a, T: buffered_reader::BufferedReader<()>> {
	pub root: &'a mut crate::Deserializer<T>,
}

impl<'d, 'a, T: buffered_reader::BufferedReader<()>> serde::de::EnumAccess<'d> for EnumDeserializer<'a, T> {
	type Error = crate::DeserializeError;
	type Variant = Self;

	fn variant_seed<K: serde::de::DeserializeSeed<'d>>(self, seed: K) -> Result<(K::Value, Self), crate::DeserializeError> {
		let mut b = self.root.input.read_to(b':')?;
		debug_assert!(b.ends_with(b":"));
		b = &b[..b.len()-1];

		if let Some(i) = b.iter().rev().position(|c| c == &b'\n') {
			// There are some comment lines we need to ignore.
			let b_len = b.len();
			self.root.input.consume(b_len - i);
			return self.variant_seed(seed)
		}

		let key = seed.deserialize(crate::KeyDeserializer {
			root: self.root,
		})?;

		Ok((key, self))
	}
}

impl<'d, 'a, T: buffered_reader::BufferedReader<()>> serde::de::VariantAccess<'d> for EnumDeserializer<'a, T> {
	type Error = crate::DeserializeError;

	fn unit_variant(self) -> Result<(), crate::DeserializeError> {
		unreachable!("unit_variant")
	}

	fn newtype_variant_seed<S: serde::de::DeserializeSeed<'d>>(self, seed: S)
	-> Result<S::Value, crate::DeserializeError>
	{
		seed.deserialize(self.root)
	}

	fn tuple_variant<V: serde::de::Visitor<'d>>(self, _len: usize, visitor: V)
	-> Result<V::Value, crate::DeserializeError>
	{
		serde::Deserializer::deserialize_seq(self.root, visitor)
	}

	fn struct_variant<V: serde::de::Visitor<'d>>(
		self,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, crate::DeserializeError>
	{
		serde::Deserializer::deserialize_map(self.root, visitor)
	}
}
